package com.swagger.app.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
public class MyController {

	@RequestMapping(value = "/authorization", method = RequestMethod.GET)
	@ApiOperation(value = "authorization", notes = "transaccion 200.")
	public String hola(@PathVariable("name")String name) {
		return "Hola Mundo: " + name; 
	}
}
